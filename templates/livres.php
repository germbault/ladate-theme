<?php

/**
 *Template Name: Test Template
 */

get_header();
// get_template_part("template-parts/page", "newsletter");
the_title();

// requete custom pour aller chercher les 10 premier livre dans la bd et les afficher dans la page
$args = array(
    'post_type'      => 'livres',
    'posts_per_page' => 10,
);
$loop = new WP_Query($args);
//************ */

if ($loop->have_posts()) { // si y a du contentue
    while ($loop->have_posts()) { // tant que y a du contenue
        $loop->the_post();
        the_title(); // titre du post_type (livres)
        the_content(); // affiche le contenue
    } // end while
} // end if
get_footer();
