<?php
get_header();
get_template_part("template-parts/page", "newsletter");
$titre = get_the_title(); // 'imprimme pas et retoure que la valeur du h1
echo "<h1 style='font-size:28px;'>" . $title . "</div>";
if (have_posts()) { // si y a du contentue
    while (have_posts()) { // tant que y a du contenue
        the_post();
        the_content(); // affiche le contenue
    } // end while
} // end if
get_footer();
